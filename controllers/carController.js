const db = require("../models");

// image Upload
const multer = require("multer");
const path = require("path");

// create main Model
const Car = db.cars;

// main work

// 1. create car
const addCar = async (req, res) => {
  let info = {
    image: req.file.path,
    car_name: req.body.car_name,
    car_type: req.body.car_type,
    price: req.body.price,
    car_size: req.body.car_size,
  };

  const car = await Car.create(info);
  res.status(200).send(car);
  console.log(car);
};

// 2. get all cars

const getAllCars = async (req, res) => {
  let cars = await Car.findAll({});
  res.status(200).send(cars);
};

// 3. update car

const updateCar = async (req, res) => {
  let id = req.params.id;

  const car = await Car.update(req.body, { where: { id: id } });

  res.status(200).send(car);
};

// 5. delete car by id

const deleteCar = async (req, res) => {
  let id = req.params.id;

  await Car.destroy({ where: { id: id } });

  res.status(200).send("Car is deleted !");
};

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "Images");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + path.extname(file.originalname));
  },
});

const upload = multer({
  storage: storage,
  limits: { fileSize: "1000000" },
  fileFilter: (req, file, cb) => {
    const fileTypes = /jpeg|jpg|png|gif/;
    const mimeType = fileTypes.test(file.mimetype);
    const extname = fileTypes.test(path.extname(file.originalname));

    if (mimeType && extname) {
      return cb(null, true);
    }
    cb("Give proper files formate to upload");
  },
}).single("image");

module.exports = {
  addCar,
  getAllCars,
  updateCar,
  deleteCar,
  upload,
};
